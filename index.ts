import * as express from 'express';
import * as mongoose from "mongoose";
import User from './models/user';
import * as ExcelJs from 'exceljs';
import * as moment from "moment";

const app = express();
app.listen(5000, () => {
    console.log('started my server');
});

mongoose.connect('mongodb+srv://shagun:s' +
    'hagun@cluster0.65wik.mongodb.net/test?' +
    'retryWrites=true&w=majority', {useNewUrlParser: true})
    .then((data) => {
        console.log('connected to database');
    });


app.get('/sheet', async (req, res, next) => {
    const startDate = moment(new Date()).startOf('month').toDate();
    const endDate = moment(new Date()).endOf('month').toDate();
    try {
        const users = await User.find({created_at:{$gte: startDate, $lte: endDate}});
        const workbook = new ExcelJs.Workbook();
        const worksheet = workbook.addWorksheet('My Users');
        worksheet.columns = [
            {header: 'S.no', key: 's_no', width: 10},
            {header: 'Email', key: 'email', width: 10},
            {header: 'name', key: 'name', width: 10},
            {header: 'onboarding', key: 'onboarding', width: 10},
            {header: 'verified', key: 'verified', width: 10},
            {header: 'job_category', key: 'job_category', width: 10},
            {header: 'experience_level', key: 'experience_level', width: 10},
        ];
        let count = 1;
        users.forEach(user => {
            (user as any).s_no = count;
            worksheet.addRow(user);
            count += 1;
        });
        worksheet.getRow(1).eachCell((cell) => {
            cell.font = {bold: true};
        });
        const data = await workbook.xlsx.writeFile('users.xlsx')
        res.send('done');
    } catch (e) {
        res.status(500).send(e);
    }
});
